import axios from "axios";
import {AJAX_CONFIG, API_URL} from "../components/App/constants";
import {changeVkId} from "./functions";

export function getNewRequestId() {
  return (Math.floor(Math.random() * Number.MAX_SAFE_INTEGER)).toString();
}

export async function auth(id, params, is_landing) {

  let res = await axios.get(API_URL + changeVkId(params, id) + '&method=user.auth&landing=' + is_landing, AJAX_CONFIG);

  console.log(res);

  if (res.data.code !== 'ok') {
    console.log(res);
    return false;
  }

  return res.data.response;
}

export async function userUpdate(id, token, business) {

  let res = await axios.get(API_URL + '?method=user.update&vk_user_id=' + id +
    '&first_name=' + business.first_name + '&last_name=' + business.last_name + '&second_name=' + business.second_name
    + '&inn=' + business.inn + '&phone=' + business.phone + '&email=' + business.email + '&btype=' + business.type + '&token=' + token, AJAX_CONFIG);

  return res.data.code === 'ok';
}