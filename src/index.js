import "core-js/features/map";
import "core-js/features/set";
import React from "react";
import ReactDOM from "react-dom";
import bridge from "@vkontakte/vk-bridge";
import AppContainer from "./components/App/AppContainer";
import * as router from './router/router';

bridge.send('VKWebAppInit');

const params = window.location.search;
const route = router.initialize();

ReactDOM.render(<AppContainer router={route} params={params} />, document.getElementById("root"));
if (process.env.NODE_ENV === "development") {
  import("./eruda").then(({ default: eruda }) => {}); //runtime download
}
