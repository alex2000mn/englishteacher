import React, {useEffect, useContext, Fragment} from 'react';
import bridge from '@vkontakte/vk-bridge';
import {useRoute} from 'react-router5';
import View from '@vkontakte/vkui/dist/components/View/View';
import {Panel, PopoutWrapper, ScreenSpinner} from "@vkontakte/vkui";
import Context from "./context";
import {pages} from "../../router/router";
import {API_VERSION, APP_ID} from "./constants";
import {auth, getNewRequestId} from "../../actions/backend";
import LoadScreen from "../../panels/LoadScreen/LoadScreen";

import './App.css';
import EnterScreen from "../../panels/EnterScreen/EnterScreen";

const App = ({params}) => {

  const {user, setUser, popout, setPopout, activePanel, changeRoute, setGroups } = useContext(Context);
  const {router, route} = useRoute();

  useEffect(() => {


    router.subscribe(changeRoute);
    changeRoute({route});

    bridge.subscribe(async ({detail: {type, data}}) => {

    });

    async function fetchData() {
      let newUser = await bridge.send('VKWebAppGetUserInfo');
      let payload = await bridge.send("VKWebAppSendPayload", {"group_id": 196045367, "payload": {"foo": "bar"}});
      const authData = await auth(newUser.id, params, window.location.hash == '#from=landing' ? 1 : 0);

      if (!authData) return;

      newUser.status = true;
      newUser.token = authData.token;
      newUser.i_array = authData.i_array;
      if (authData.group_id && authData.group_id != '0') newUser.group_id = authData.group_id;
      newUser.activity = authData.activity;

      setUser(newUser);

      if (authData.status == 1) {
       // router.navigate(pages.BUSINESS_CARD, {}, {replace: true});
      } else router.navigate(pages.ENTER_SCREEN, {}, {replace: true});
    }

    fetchData();
  }, []);

  if (!activePanel) {

    return null;
  }

  return (
    <Fragment>
      <View activePanel={activePanel} popout={popout} header={false}>
        <Panel id={pages.LOAD_SCREEN}>
          <LoadScreen/>
        </Panel>
        <Panel id={pages.ENTER_SCREEN}>
          <EnterScreen/>
        </Panel>
      </View>
    </Fragment>
  );

};

export default App;