import React, {Fragment, useEffect} from 'react';

import './EnterScreen.css';
import {useRouter} from "react-router5";
import {pages} from "../../router/router";
import bridge from "@vkontakte/vk-bridge";

const EnterScreen = () => {

  useEffect(() => {
    bridge.send("VKWebAppSetViewSettings", {"status_bar_style": "dark", "action_bar_color": "#fff"});
  }, []);

  const router = useRouter();
  const goToProfile = () => router.navigate(pages.PROFILE, {}, {replace: true});

  return (
    <Fragment>
      <div className="EnterScreen">
        <div className="english"></div>
        <div className="text">
          Привет! Это приложение для изучения английского языка!<br/>
          Здесь вы можете:<br/>
          -проходить тесты на знание английского языка;<br/>
          -изучать новые слова;<br/>
          -отслеживать прогресс;<br/>
        </div>
        <div className="button">НАЧАТЬ</div>
      </div>
    </Fragment>
  )
};

export default EnterScreen;