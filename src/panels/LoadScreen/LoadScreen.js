import React, {Fragment, useEffect, useState} from 'react';

import './LoadScreen.css';
import bridge from "@vkontakte/vk-bridge";

const LoadScreen = () => {

  const [activeBlock, setActiveBlock] = useState(0);

  useEffect(() => {
    bridge.send("VKWebAppSetViewSettings", {"status_bar_style": "light", "action_bar_color": "#F52C44"});
  }, []);

  useEffect(() => {
    let id = setInterval(() => {
      let newValue = activeBlock == 3 ? 0 : activeBlock + 1;
      setActiveBlock(newValue);
    }, 250);
    return () => clearInterval(id);
  });

  const getBlockStyle = (id) => {
    if (id == activeBlock) return "block block--active";
    return "block";
  }

  return (
    <Fragment>
      <div className="LoadScreen">
        <div className="loadingBlockContainer">
          <div className="row">
            <div className={getBlockStyle(0)}/>
            <div className={getBlockStyle(1)}/>
          </div>
          <div className="row">
            <div className={getBlockStyle(3)}/>
            <div className={getBlockStyle(2)}/>
          </div>
        </div>
      </div>
    </Fragment>
  )
};

export default LoadScreen;